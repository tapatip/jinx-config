## jinx-config
A basic Nginx config file generation utility. Accepts a limited number of command line arguments and outputs an Nginx config file to support (for instance) an Nginx+Passenger configuration for use in a Docker container.

## Why?
There's no easy way to create an Nginx configuration file from scratch using an automated process. For instance, to create an Nginx config from from within a Dockerfile comes down to the following two options:

* copy a pre-configured file using the `ADD` or `COPY` command
* running a script using the `RUN` command
* running a number of `sed` instructions via the `RUN` command

All of the above strategies have problems in the case of a Dockerfile. Copying a pre-configured file is compatible with the Dockerfile caching mechanism but it requires you to hardcode configuration details beforehand. Running a script to generate a file seems like a suitable alternative but it isn't compatible with the Dockerfile caching mechanism. Running a bunch of `sed` commands can easily become an incomprehensible mess as complexity increases.

With jinx-config you can generate an Nginx config file to support an Nginx+Passenger configuration using command line arguments used alone or in conjunction with an _INI_ file to support dynamic customization. This technique supports the Dockerfile caching mechanism (if the command line option arguments change, then the cache is invalidated). An _INI_ configuration file can also be used on its own and if created with [eof-append](https://bitbucket.org/tapatip/eof-append) this approach also supports Dockerfile caching.

## Installation
To install jinx-config, just copy the entire script directory (including the templates, documentation and examples directories) to a suitable directory (like /usr/local/jinx-config) as the superuser. Then, execute the following permissions changes:

```sh
>cd /path/to/install/directory (e.g. cd /usr/local)
>chown -R root:root jinx-config
>chmod 755 jinx-config/jinx-config.sh
>ln -s jinx-config.sh jinx-config
>ln -s $PWD/jinx-config.sh /usr/local/bin/jinx-config
```

After the execute bit has been set on the jinx-config.sh script, you will be able to call the script without having to invoke a shell first and then pass the script as an argument. A symbolic link is created in the /usr/local/bin directory so that the command becomes visible in your shell PATH.

**NOTE: If /usr/local/bin is not visible in your PATH, you will need to add it.**

**NOTE: jinx-config requires BASH 4.2 or greater.**

### Installation for Dockerfile
To use jinx-config with Dockerfile, you will need to copy the script into an executable place during the build process.

```sh
RUN mkdir -p /build/bin /build/tmp
WORKDIR /build
RUN curl -sSL -o tmp/jinx-config.bz2 https://bitbucket.org/tapatip/jinx-config/get/v1.0.6.tar.bz2 \
  && arc="tmp/jinx-config.bz2"; file="jinx-config.sh"; tmpl="templates/$" \
    file="$(tar tjf ${arc} | grep ${file})"; \
    tmpl="$(tar tjf ${arc} | grep ${tmpl})"; \
    dir="$(dirname ${file})" \
    && tar xjf $arc -C tmp $file && cp tmp/$file bin \
    && tar xjf $arc -C tmp $tmpl && cp -R tmp/$tmpl bin
```

The above line will extract only the script and template file to provide a minimal install for use in a Docker build process. The installed script is then called from a Dockerfile with its full path:

```sh
RUN bash -c '/build/bin/jinx-config.sh -x \
  --app-user webapp \
  --app-root /home/webapp \
  --out-file /usr/local/nginx/conf/nginx.conf '
```

## Usage
When specifying the `-c | -- use-config` option and supplying an INI config file (see the examples directory), the file path can be provided as the final argument or its contents can be supplied to the jinx-config command using the redirection operator `<`. For instance, both of the following invocations are supported:

```sh
>jinx-config -c -x -l debug my-config-file.ini

-OR-

>jinx-config -c -x -l debug < my-config-file.ini
```

For help, just invoke jinx-config with the help `(-h | --help)` option:

```sh
>jinx-config --help
```

Options include:

```sh
   -c, --use-config             Parse config file containing keys/vals
   -i, --in-file=path           Input file (path)
   -o, --out-file=path          Output file (path)
   -e, --app-penv=environment   App server production environment:
                                  - "development" (default)
                                  - "production"
                                  - "staging"
   -n, --app-name=hostname      App server hostname (default: localhost)
   -u, --app-user=user          App server user id (default: nobody)
   -w, --app-root=path          App server root (path, default: /var/www)
   -p, --passenger-root=path    Path to passenger root
   -r, --passenger-ruby=path    Path to passenger ruby
   -l, --log-level=level        Log level (affects error log only):
                                  - "debug"
                                  - "info"
                                  - "notice"
                                  - "warn"
                                  - "error"
                                  - "crit"
                                  - "alert"
                                  - "emerg"
   -s, --log-stdout             Log to stdout
   -x, --log-disable            Disable access log
   -d, --debug                  Turn debugging on (increases verbosity)
   -f, --force                  Execute without user prompt
   -h, --help                   Show this message
   -v, --version                Output version of this script
   -z, --show-settings          Show final settings that will be applied
```

### Config file
In addition to (or instead of) command line configuration options you can also provide an INI format config file as input to the jinx-config command. An example config file is provided in the utility's examples directory.

### Input file
The jinx-config utility works its magic by replacing a small number of predefined tokens that appear in a configuration template file. The default template file is located in the utility's templates directory. You can create your own template file and specify its location using the `-i | --in-file` command line option.

### Output file
The generated configuration file will be copied to the default path (see _Show Settings_) or to the path specified by the `-o | --out-file` option. If the file exists, it will be truncated; if the file doesn't exist, it will be created.

### Show settings
To view merged values without doing anything else, simply add the `-z | --show-settings` option to your command line.

## Merging of values
The order in which configuration values will be merged is as follows left to right:

```
Defaults->INI file->Command line parameters
```

INI file parameters override defaults. Command line parameters override all previous values. To view merged values without doing anything else, simply add the `-z | --show-settings` option to your command line.

## Limitations
The jinx-config utility is meant to be simple. It supports a limited number of customization parameters (more may be added later and if this happens it's likely they would only be added to the INI file--you can only have so many command line options after all).

## Compatibility
jinx-config has been developed and tested on OSX and Linux. Supported Linux distributions currently include:

* [CoreOS](https://coreos.com/)
* [CentOS](http://www.centos.org/)

jinx-config requires BASH 4.2.0 or higher.

## License
jinx-config is licensed under the MIT open source license.
