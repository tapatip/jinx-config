#!/usr/bin/env bash
#
# STEmacsModelines:
# -*- Shell-Unix-Generic -*-
#
# /*!
# @header jinx-config.sh
# Generate an Nginx+Passenger server config (for Docker).
# @author Mark Eissler (mark\@mixtur.com)
# */
#
# To build documentation for this file:
# >headerdoc2html -c doc/headerdoc.cfg -o doc jinx-config.sh
#

# Copyright (c) 2014 Mark Eissler, mark@mixtur.com

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

PATH=/usr/local/bin

# utility paths (adjust for your local system if needed)
#
# NOTE: Some of these will be updated automatically if OSX is detected.
#
g_path_getopt="/usr/bin/getopt"
g_path_cat="/bin/cat"
g_path_sed="/usr/bin/sed"
g_path_tr="/usr/bin/tr"
g_path_mkdir="/bin/mkdir"
g_path_rm="/bin/rm"
g_path_touch="/usr/bin/touch"
g_path_basename="/usr/bin/basename"
g_path_dirname="/usr/bin/dirname"
g_path_pwd="/bin/pwd"

###### NO SERVICABLE PARTS BELOW ######
g_version=1.0.6
g_progname=$(${g_path_basename} $0)

# reset internal vars (do not touch these here)
g_debug=0
g_show_vars=0
g_getopt_old=0
g_forceexec=0
g_emptystr=""
g_path_script="$(cd "$(${g_path_dirname} "${BASH_SOURCE[0]}")" && ${g_path_pwd})"
g_path_infile="${g_path_script}/templates/nginx.conf.tmpl"
g_path_outfile=""
g_nginx_access_log="/tmp/nginx_access.log"
g_nginx_error_log="/tmp/nginx_error.log"
g_pass_root="/usr/local/rubygems/passenger"
g_pass_ruby="/usr/bin/ruby"
g_app_server_name="localhost"
g_app_server_root="/var/www"
g_app_server_user="nobody"
g_app_server_penv="development"
g_log_stdout=0
g_log_disable=0
g_log_level="info"
g_use_config=0
g_path_worktemp="/tmp/jinx"
g_path_stdout="/dev/stdout"

# min bash required
g_vers_bash_major=4
g_vers_bash_minor=2
g_vers_bash_patch=0

# check for minimum bash
if [[ ${BASH_VERSINFO[0]} < ${g_vers_bash_major} ||
  ${BASH_VERSINFO[1]} < ${g_vers_bash_minor} ||
  ${BASH_VERSINFO[2]} < ${g_vers_bash_patch} ]]; then
  echo -n "${PROGNAME} requires at least BASH ${g_vers_bash_major}.${g_vers_bash_minor}.${g_vers_bash_patch}!"
  echo " (I seem to be running in BASH ${BASH_VERSINFO[0]}.${BASH_VERSINFO[1]}.${BASH_VERSINFO[2]})"
  echo
  exit 100
fi

#
# FUNCTIONS
#

# usage()
# /*!
# @abstract Output correct help depending on installed getopt() functionality
# @discussion
# Installed version of getopt utility may support short options or long and
#   short options. Calling this function will output the appropriate usage info.
#   The global "$g_getopt_old" variable must be defined.
# */
function usage {
  if [ ${g_getopt_old} -eq 1 ]; then
    echo "usage: ${g_progname} [-d] [options] [-c config-file]"
    echo "usage: ${g_progname} [-d] -h | -v"
  else
    echo "usage: ${g_progname} [-d|--debug] [options] [-c config-file]"
    echo "usage: ${g_progname} [-d|--debug] -h | --help"
    echo "usage: ${g_progname} [-d|--debug] -v | --version"
  fi

  echo
}

# help()
# /*!
# @abstract Output correct help depending on installed getopt() functionality
# @discussion
# Installed version of getopt utility may support short options or long and
#   short options. Calling this function will output the appropriate usage info.
#   The global "$g_getopt_old" variable must be defined.
# */
function help {
  if [ ${g_getopt_old} -eq 1 ]; then
    help_old
  else
    help_new
  fi
}

# help_new()
# /*!
# @abstract Output verbose usage info that includes long option flags
# @discussion
#   This method shouldn't be called directly. Call help() instead.
# @internal
# */
function help_new {
usage

${g_path_cat} << EOF
Generate an Nginx configuration with support for Phusion Passenger integration.

OPTIONS:
   -c, --use-config             Parse config file containing keys/vals
   -i, --in-file=path           Input file (path)
   -o, --out-file=path          Output file (path)
   -e, --app-penv=environment   App server production environment:
                                  - "development" (default)
                                  - "production"
                                  - "staging"
   -n, --app-name=hostname      App server hostname (default: localhost)
   -u, --app-user=user          App server user id (default: nobody)
   -w, --app-root=path          App server root (path, default: /var/www)
   -p, --passenger-root=path    Path to passenger root
   -r, --passenger-ruby=path    Path to passenger ruby
   -l, --log-level=level        Log level (affects error log only):
                                  - "debug"
                                  - "info"
                                  - "notice"
                                  - "warn"
                                  - "error"
                                  - "crit"
                                  - "alert"
                                  - "emerg"
   -s, --log-stdout             Log to stdout
   -x, --log-disable            Disable access log
   -d, --debug                  Turn debugging on (increases verbosity)
   -f, --force                  Execute without user prompt
   -h, --help                   Show this message
   -v, --version                Output version of this script
   -z, --show-settings          Show final settings that will be applied

Passenger root (-p, --passenger-root)
-------------------------------------
The passenger root path must point to the passenger binary. For a multi-
user ruby rvm installation this will likely be somewhere under /usr/local/ and
usually looks like a path with the following form:
  "/usr/local/rvm/gems/ruby-2.1.2@myapp/gems/passenger-4.0.37;"

For a user single-user ruby rvm installation the path will be located somewhere
under the associated user home directory similar to this:
  "/home/USER/.rvm/gems/ruby-2.1.2@myapp/gems/passenger-4.0.37;"

If this parameter is not specified, the default passenger root path will be set
to /usr/local/rubygems/passenger. In most cases this will be incorrect.

Passenger ruby (-r, --passenger-ruby=path)
------------------------------------------
The passenger ruby path must point to a ruby binary. For a multi-user ruby rvm
installation this will likely be somewhere under /usr/local/ and usually looks
like a path with the following form:
  "/usr/local/rvm/wrappers/ruby-2.1.2@myapp/ruby;"

For a user single-user ruby rvm installation the path will be located somewhere
under the associated user home directory similar to this:
  "/home/USER/.rvm/gems/ruby-2.1.2@myapp/wrappers/ruby;"

If this parameter is not specified, the default passenger ruby path will be set
to /usr/bin/ruby.

Use config file (-c, --use-config)
----------------------------------
When the config file option is enabled you must specify a config file to parse
as the final argument or use redirection as input.

Input and Output files
----------------------
An alternative base Nginx configuration input file can be specified (with the
--in-file option) and features embedded that will be replaced by default values
or parameters passed to this script. If no input alternative input file is
specified, the default input file will be used. Output will be to stdout if no
output file is specified (with the --out-file option).

Parameter value priority
------------------------
In general, values passed as cli parameters take precedence over those passed in
an input-file; values passed in an input file take precedence over those defined
as defaults: cli->ini->defaults

EOF
}

# help_old()
# /*!
# @abstract Output help info that does not include long option flags
# @discussion
#   This method shouldn't be called directly. Call help() instead.
# @internal
# */
function help_old {
usage

${g_path_cat} << EOF
Generate an Nginx configuration with support for Phusion Passenger integration.

OPTIONS:
   -c                           Parse config file containing keys/vals
   -i                           Input file (path)
   -o                           Output file (path)
   -e                           App server production environment:
                                  - "development" (default)
                                  - "production"
                                  - "staging"
   -n                           App server hostname (default: localhost)
   -u                           App server user id (default: nobody)
   -w                           App server root (path, default: /var/www)
   -p                           Path to passenger root
   -r                           Path to passenger ruby
   -l                           Log level (affects error log only):
                                  - "debug"
                                  - "info"
                                  - "notice"
                                  - "warn"
                                  - "error"
                                  - "crit"
                                  - "alert"
                                  - "emerg"
   -s                           Log to stdout
   -x                           Disable access log
   -d                           Turn debugging on (increases verbosity)
   -f                           Execute without user prompt
   -h                           Show this message
   -v                           Output version of this script
   -z                           Show final settings that will be applied

Passenger root (-p)
-------------------
The passenger root path must point to the passenger binary. For a multi-
user ruby rvm installation this will likely be somewhere under /usr/local/ and
usually looks like a path with the following form:
  "/usr/local/rvm/gems/ruby-2.1.2@myapp/gems/passenger-4.0.37;"

For a user single-user ruby rvm installation the path will be located somewhere
under the associated user home directory similar to this:
  "/home/USER/.rvm/gems/ruby-2.1.2@myapp/gems/passenger-4.0.37;"

If this parameter is not specified, the default passenger root path will be set
to /usr/local/rubygems/passenger. In most cases this will be incorrect.

Passenger ruby (-r)
-------------------
The passenger ruby path must point to a ruby binary. For a multi-user ruby rvm
installation this will likely be somewhere under /usr/local/ and usually looks
like a path with the following form:
  "/usr/local/rvm/wrappers/ruby-2.1.2@myapp/ruby;"

For a user single-user ruby rvm installation the path will be located somewhere
under the associated user home directory similar to this:
  "/home/USER/.rvm/gems/ruby-2.1.2@myapp/wrappers/ruby;"

If this parameter is not specified, the default passenger ruby path will be set
to /usr/bin/ruby.

Use config file (-c)
--------------------
When the config file option is enabled you must specify a config file to parse
as the final argument or use redirection as input.

Input and Output files
----------------------
An alternative base Nginx configuration input file can be specified (with the
-i option) and features embedded that will be replaced by default values
or parameters passed to this script. If no input alternative input file is
specified, the default input file will be used. Output will be to stdout if no
output file is specified (with the -o option).

Parameter value priority
------------------------
In general, values passed as cli parameters take precedence over those passed in
an input-file; values passed in an input file take precedence over those defined
as defaults: cli->ini->defaults

EOF
}

# mkTempFile()
# /*!
# @abstract Create a temporary filename
# @discussion
# The filename will be prepended with the "/tmp" if a directory is not specified
#   and the current directory has not been indicated. The generated filename
#   will be prepended with the process PID if a filename prefix is not specified
#   and the option to omit the pid has not been indicated. If calling this
#   function recursively, specify all parameters including the collision guard,
#   an integer that is incremented on each pass to avoid race condtions.
#
#   By default, mkTempDir() operates in safe mode and will create the underlying
#   directory; this behavior can be disabled by declaring the TMPMK global
#   variable and setting it to a value other than zero. This can be done
#   temporarily like this:
#       $(TMPMK=0 mkTempDir)
#   In the above example, a subshell is spawned but this is not necessary. Of
#   course, you can also just set TMPMK=0 at the global level of your script.
# @param tempDir The directory or "-" to use the current directory.
# @param prependString A string to prepend (a "prefix") or "-" to omit the PID.
# @param collisionGuard Collision guard
# @return Generated filename, emptry string on error.
# */
function mkTempFile() {
  local tempFile=""
  local tempDir=""
  if [ "${1}" != "-" ]; then
    tempDir=${1}${1:+/}
    tempDir=${tempDir:-/tmp/}
  fi

  local collisionGuard=0
  if [ -n "${3}" ] && [ "${3}" -gt 0 ]; then
    collisionGuard=${3}
  fi

  while [[ $collisionGuard -lt 5 ]]; do
    if [ "${2}" != "-" ]; then
      tempFile=${2}${2:+-}
      local tempPid=$$
      tempFile=${tempFile:-${tempPid}-}
    fi
    tempFile=$tempDir$tempFile$RANDOM$RANDOM.tmp
    if [ -e $tempFile ]; then
      # if file exists, try again
      tempFile=$(mkTempFile $1 $2 $collisionGuard)
      if [ $? -ne 0 ]; then
        (( collisionGuard++ ))
        continue
      fi
    fi
    if [[ -z "${TMPMK}" || ${TMPMK} -ne 0 ]]; then
      ${g_path_mkdir} -p "${tempDir}"
      ${g_path_touch} "${tempFile}"
    fi
    echo ${tempFile}; return 0
  done

  echo ""; return 1
}

# mkTempDir()
# /*!
# @abstract Create tempory directory name
# @discussion
# The directory name will be prepended with the "/tmp" if a directory is not
#   specified and the current directory has not been indicated. The generated
#   directory name will be prepended with the process PID if a filename prefix
#   is not specified and the option to omit the pid has not been indicated. If
#   calling this function recursively, specify all parameters including the
#   collision guard, an integer that is incremented on each pass to avoid race
#   condtions.
#
#   By default, mkTempDir() operates in safe mode and will create the underlying
#   directory; this behavior can be disabled by declaring the TMPMK global
#   variable and setting it to a value other than zero. This can be done
#   temporarily like this:
#       $(TMPMK=0 mkTempDir)
#   In the above example, a subshell is spawned but this is not necessary. Of
#   course, you can also just set TMPMK=0 at the global level of your script.
# @param prependString A string to prepend (a "prefix") or "-" to omit the PID.
# @param collisionGuard Collision guard
# @return Generated filename, emptry string on error.
# */
function mkTempDir() {
  local tempDir=""
  if [ "${1}" != "-" ]; then
    tempDir=${1}${1:+/}
    tempDir=${tempDir:-/tmp/}
  fi

  local collisionGuard=0
  if [ -n "${3}" ] && [ "${3}" -gt 0 ]; then
    collisionGuard=${3}
  fi

  while [[ $collisionGuard -lt 5 ]]; do
    if [ "${2}" != "-" ]; then
      tempSubDir=${2}${2:+-}
      local tempPid=$$
      tempDir=${tempDir}${tempSubDir:-${tempPid}-}
    fi

    tempDir=$tempDir$RANDOM$RANDOM
    if [ -d $tempDir ]; then
      # if directory exists, try again
      tempDir=$(mkTempDir $1 $2 $collisionGuard)
      if [ $? -ne 0 ]; then
        (( collisionGuard++ ))
        continue
      fi
    fi

    if [[ -z "${TMPMK}" || ${TMPMK} -ne 0 ]]; then
      ${g_path_mkdir} -p "${tempDir}"
    fi
    echo ${tempDir}; return 0
  done

  echo ""; return 1
}

# promptConfirm()
# /*!
# @abstract Confirm a user action. Input case insensitive
# @discussion
# Read a simple case-insensitive [y]es or [N]o prompt, where "No" is the default
#   if no answer is entered by the user. This function does not present a
#   prompt, it only reads the response.
# @return Returns string containiner "yes" or "no".
# */
function promptConfirm() {
  read -p "$1 ([y]es or [N]o): "
  case $(echo $REPLY | ${g_path_tr} '[A-Z]' '[a-z]') in
    y|yes) echo "yes" ;;
    *)     echo "no" ;;
  esac
}

# version()
# /*!
# @abstract Output version information
# @discussion
# Output a version string on the first line, any other version information on
#   additional lines.
# */
function version {
  echo ${g_progname} ${g_version};
}

# isNumber()
# /*!
# @abstract Check if a given string is a number
# @discussion
# Parses a string to test whether its components are digits including decimals.
#   The string can be preceded by a negative sign to indicate a negative value.
# @param numberStr String to check.
# @return Returns 1 on success (status 0), 0 on failure (status 1).
# */
isNumber() {
  if [[ ${1} =~ ^-?[0-9]+([.][0-9]+)?$ ]]; then
    # match
    echo 1; return 0
  else
    # no match
    echo 0; return 1
  fi
}

# isPathWriteable()
# /*!
# @abstract Checks if a given path is writeable by the current user
# @discussion
# Tests whether a given path (file or directory) is writable by the current
#   user.
# @return Returns 1 on success (status 0), 0 on failure (status 1).
# */
isPathWriteable() {
  if [ -z "${1}" ]; then
    echo 0; return 1
  fi

  # path is a directory...
  if [[ -d "${1}" ]]; then
    local path="${1%/}/.test"
    local resp rslt
    resp=$({ ${g_path_touch} "${path}"; } 2>&1)
    rslt=$?
    if [[ ${rslt} -ne 0 ]]; then
      # not writeable directory
      echo 0; return 1
    else
      # writeable directory
      ${g_path_rm} "${path}"
      echo 1; return 0
    fi
  fi

  # path is a file...
  if [ -w "${1}" ]; then
    # writeable file
    echo 1; return 0
  else
    # not writeable file
    echo 0; return 1
  fi

  # and if we fall through...
  echo 0; return 128
}

# readConfigFromArray()
# /*!
# @abstract Parse an array of KEY=VAL strings into an assoc array
# @discussion
# Iterates over each row of the array, parses the KEY=VAL definitions contained
#   in the strings, and writes an associative array to a global var passed by
#   name in outputVarName.
#   The outputVarName must not be quoted by the caller:
#       readConfigFromArray myOutputVar inputArray[\@]
#
#   In the above example, the caller can then access values:
#       ${myOutputVar[KEY]}
#
#   Sections are supported and follow the [SECTION_NAME] syntax. Section names
#   must appear on lines by themselves and precede the KEY=VAL definictions that
#   they contain. Section names will be converted to uppercase. For flexibility,
#   a pseudo-multidimensional associative array is created by using the
#   following key format:
#       [SECTION_NAME,KEY]=VAL
#
#   By default, the SECTION_NAME and KEY name parts will be delimited with a
#   comma as shown above; however, an alternate character (or string) can be set
#   as a delimiter by declaring the INIDM global variable and setting it to the
#   preferred value. This can be done temporarily like this:
#       INIDM='_' readConfigFromFile myOutputVar /path/to/file
#
#   Of course, you can also just set INIDM at the global level of your script.
#
#   All KEY=VAL definitions that should appear outside of any section (that is,
#   at the global level) must appear before any section name is encountered.
#
#   Neither [SECTION_NAME] or KEY=VAL definitions can be followed by comments.
#
# @param outputVarName Name of global variable into which output will be copied.
# @param inputArray Array containing strings of KEY=VAL pairs.
# */
readConfigFromArray() {
  local _outputVarName="$1"
  local _inputArray=("${!2}")
  local _key _val
  local _assignOp='='
  local _lineRegex="^[^#;]*[^${_assignOp}][${_assignOp}]{1}[^${_assignOp}]*$"
  # assoc array (i.e. section label) support
  local _sectionNameBeg='['
  local _sectionNameEnd=']'
  local _lineRegexArr="^[${_sectionNameBeg}]{1}[[:alnum:]\_\-]+[${_sectionNameEnd}]{1}$"
  local _sectionNameKey=""
  local _sectionNameKeyDelim=','
  local _count=0
  declare -g -A "$_outputVarName"
  if [[ ! -z ${INIDM+x} ]]; then
    _sectionNameKeyDelim="${INIDM}"
  fi
  if [[ ${g_debug} -ne 0 ]]; then
    echo "DEBUG ($FUNCNAME): _inputArray length: ${#_inputArray[@]}"
  fi
  for ((i=0; i<${#_inputArray[@]}; i++)); do
    _line="${_inputArray[$i]}"
    if [[ "${_line}" =~ ${_lineRegexArr} ]]; then
      # convert label to uppercase, extract part between square brackets
      local __label="${_line^^}"; __label=${__label:1:-1}
      _sectionNameKey=$__label
      unset __label
    fi
    # assume comments may not be indented
    if [[ "${_line}" =~ ${_lineRegex} ]]; then
      IFS=${_assignOp} read _key _val <<< "$_line"
      [[ -n $_key ]] || continue
      # convert key to uppercase
      _key="${_key^^}"
      # add array label to key
      if [[ -n "${_sectionNameKey}" ]]; then
        _key="${_sectionNameKey}${_sectionNameKeyDelim}${_key}"
      fi
      if [[ ${g_debug} -ne 0 ]]; then
        (( _count++ ))
        printf "DEBUG ($FUNCNAME): %02s [${_key}]: ${_val}\n" "${_count}"
      fi
      eval "IFS=${_assignOp} $_outputVarName[${_key}]=${_val}"
    fi
  done
}

# readConfigFromFile()
# /*!
# @abstract Parse a config file with KEY=VAL definitions into an assoc array
# @discussion
# Reads the KEY=VAL definitions from the file pointed to by inputFilePath and
#   writes an associative array to a global var passed by name in outputVarName.
#   The outputVarName must not be quoted by the caller:
#       readConfigFromFile myOutputVar /path/to/file
#
#   In the above example, the caller can then access values:
#       ${myOutputVar[KEY]}
#
#   Sections are supported and follow the [SECTION_NAME] syntax. Section names
#   must appear on lines by themselves and precede the KEY=VAL definictions that
#   they contain. Section names will be converted to uppercase. For flexibility,
#   a pseudo-multidimensional associative array is created by using the
#   following key format:
#       [SECTION_NAME,KEY]=VAL
#
#   By default, the SECTION_NAME and KEY name parts will be delimited with a
#   comma as shown above; however, an alternate character (or string) can be set
#   as a delimiter by declaring the INIDM global variable and setting it to the
#   preferred value. This can be done temporarily like this:
#       INIDM='_' readConfigFromFile myOutputVar /path/to/file
#
#   Of course, you can also just set INIDM at the global level of your script.
#
#   All KEY=VAL definitions that should appear outside of any section (that is,
#   at the global level) must appear before any section name is encountered.
#
#   Neither [SECTION_NAME] or KEY=VAL definitions can be followed by comments.
# @param outputVarName Name of global variable into which output will be copied.
# @param inputFilePath Path to input file.
# */
readConfigFromFile() {
  local _outputVarName="$1"
  local _inputFilePath="$2"
  local _key _val
  # line key/val support
  local _assignOp='='
  local _lineRegex="^[^#;]*[^${_assignOp}][${_assignOp}]{1}[^${_assignOp}]*$"
  # assoc array (i.e. section label) support
  local _sectionNameBeg='['
  local _sectionNameEnd=']'
  local _lineRegexArr="^[${_sectionNameBeg}]{1}[[:alnum:]\_\-]+[${_sectionNameEnd}]{1}$"
  local _sectionNameKey=""
  local _sectionNameKeyDelim=','
  local _count=0
  declare -g -A "$_outputVarName"
  if [[ ! -z ${INIDM+x} ]]; then
    _sectionNameKeyDelim="${INIDM}"
  fi
  if [ -n "${_inputFilePath}" ]; then
    while IFS= read _line; do
      if [[ "${_line}" =~ ${_lineRegexArr} ]]; then
        # convert label to uppercase, extract part between square brackets
        local __label="${_line^^}"; __label=${__label:1:-1}
        _sectionNameKey=$__label
        unset __label
      fi
      # assume comments may not be indented
      if [[ "${_line}" =~ ${_lineRegex} ]]; then
        IFS=${_assignOp} read _key _val <<< "$_line"
        [[ -n $_key ]] || continue
        # convert key to uppercase
        _key="${_key^^}"
        # add array label to key
        if [[ -n "${_sectionNameKey}" ]]; then
          _key="${_sectionNameKey}${_sectionNameKeyDelim}${_key}"
        fi
        if [[ ${g_debug} -ne 0 ]]; then
          (( _count++ ))
          printf "DEBUG ($FUNCNAME): %02s [${_key}]: ${_val}\n" "${_count}"
        fi
        eval "IFS=${_assignOp} $_outputVarName[${_key}]=${_val}"
      fi
    done <<< "$(${g_path_cat} ${_inputFilePath})"
  else
    while IFS= read reply; do
      if [[ "${_line}" =~ ${_lineRegexArr} ]]; then
        # convert label to uppercase, extract part between square brackets
        local __label="${_line^^}"; __label=${__label:1:-1}
        _sectionNameKey=$__label
        unset __label
      fi
      # assume comments may not be indented
      if [[ "${_line}" =~ ${_lineRegex} ]]; then
        IFS=${_assignOp} read _key _val <<< "$_line"
        [[ -n $_key ]] || continue
        # convert key to uppercase
        _key="${_key^^}"
        # add array label to key
        if [[ -n "${_sectionNameKey}" ]]; then
          _key="${_sectionNameKey}${_sectionNameKeyDelim}${_key}"
        fi
        if [[ ${g_debug} -ne 0 ]]; then
          (( _count++ ))
          printf "DEBUG ($FUNCNAME): %02s [${_key}]: ${_val}\n" "${_count}"
        fi
        eval "IFS=${_assignOp} $_outputVarName[${_key}]=${_val}"
      fi
    done
  fi
}

# dumpConfig()
# /*!
# @abstract Dump the global config array
# @discussion
# Iterate over the global config array and print keys and values. This function
#   will only produce output if the "$g_debug" variable has been set so it does
#   not need to be wrapped in a debug conditional.
# */
dumpConfig() {
  # get name of calling function
  local _funcname="${FUNCNAME[1]}"
  local _count=0

  if [[ ${g_debug} -ne 0 ]]; then
    echo "DEBUG ($_funcname) ==== gConfig"
    for key in ${!gConfig[@]}; do
      (( _count++ ))
      printf "DEBUG (${_funcname}): %02d [${key}]: ${gConfig[$key]}\n" "${_count}"
    done
  fi
}

# showSettings()
# /*!
# @abstract Show all internal settings to be applied
# @discussion
# Iterate over the global config array and print keys and value. Output any
#   other internal values that will affect results.
# */
showSettings() {
  local _count=0

  echo "==== Merged settings"
  for key in ${!gConfig[@]}; do
    (( _count++ ))
    printf "%02d ${key}: ${gConfig[$key]}\n" "${_count}"
  done
}

# init our config associative array with defaults
declare -A gConfig
gConfig=( \
  ['NGINX_ACCESS_LOG']="${g_nginx_access_log}" \
  ['NGINX_ERROR_LOG']="${g_nginx_error_log}" \
  ['NGINX_LOG_LEVEL']="${g_log_level}" \
  ['PASSENGER_ROOT']="${g_pass_root}" \
  ['PASSENGER_RUBY']="${g_pass_ruby}" \
  ['APP_SERVER_NAME']="${g_app_server_name}" \
  ['APP_SERVER_ROOT']="${g_app_server_root}" \
  ['APP_SERVER_USER']="${g_app_server_user}" \
  ['APP_SERVER_PENV']="${g_app_server_penv}" \
)

# parse cli parameters
#
# Our options:
#   --use-config, -c
#   --in-file=path, -i
#   --out-file=path, -o
#   --app-penv=environment, -e
#   --app-name=hostname, -n
#   --app-user=user, -u
#   --app-root=path, -w
#   --passenger-root=path, -p
#   --passenger-ruby=path, -r
#   --log-level=level, -l
#   --log-stdout, -s
#   --log-disable, -x
#   --debug, d
#   --force, f
#   --help, h
#   --version, v
#   --show-settings, z
#
_params=""
${g_path_getopt} -T > /dev/null
if [ $? -eq 4 ]; then
  # GNU enhanced getopt is available
  _long_opts=" \
    use-config,\
    in-file:,\
    out-file:,\
    app-penv:,\
    app-name:,\
    app-user:,\
    app-root:,\
    passenger-root:,\
    passenger-ruby:,\
    log-level:,\
    log-stdout,\
    log-disable,\
    force,\
    help,\
    version,\
    debug,\
    show-settings\
  "
  _params="$(${g_path_getopt} --name "${g_progname}" --long "${_long_opts}" --options ci:o:e:n:u:w:p:r:l:sxfhvdz -- "$@")"
  unset _long_opts
else
  # Original getopt is available
  g_getopt_old=1
  _params="$(${g_path_getopt} ci:o:e:n:u:w:p:r:l:sxfhvdz "$@")"
fi

# check for invalid params passed; bail out if error is set.
if [ $? -ne 0 ]
then
  usage; exit 1;
fi

eval set -- "$_params"
unset _params
_params_count=$#

while [ $# -gt 0 ]; do
  case "$1" in
    -c | --use-config)
      cli_USE_CONFIG=1; g_use_config=${cli_USE_CONFIG};
      ;;
    -i | --in-file)
      cli_INFILE="$2";
      shift;
      ;;
    -o | --out-file)
      cli_OUTFILE="$2";
      shift;
      ;;
    -e | --app-penv)
      _param="${2,,}" # convert to lowercase
      case "${_param}" in
        development) ;&
        production) ;&
        staging)
          cli_APP_PENV="$2"; g_app_server_penv=${_param};
          shift
          ;;
        *)
          echo "-e option value is invalid";
          usage;
          exit 1;
          ;;
      esac
      unset _param
      ;;
    -n | --app-name)
      cli_APP_NAME="$2";
      shift;
      ;;
    -u | --app-user)
      cli_APP_USER="$2";
      shift;
      ;;
    -w | --app-root)
      cli_APP_ROOT="$2";
      shift;
      ;;
    -p | --passenger-root)
      cli_PASS_ROOT="$2";
      shift;
      ;;
    -r | --passenger-ruby)
      cli_PASS_RUBY="$2";
      shift;
      ;;
    -l | --log-level)
      _param="${2,,}" # convert to lowercase
      case "${_param}" in
        debug) ;&
        info) ;&
        notice) ;&
        warn) ;&
        error) ;&
        crit) ;&
        alert) ;&
        emerg)
          cli_LOG_LEVEL="$2"; g_log_level=${_param};
          shift
          ;;
        *)
          echo "-l option value is invalid";
          usage;
          exit 1;
          ;;
      esac
      unset _param
      ;;
    -s | --log-stdout)
      cli_LOG_STDOUT=1; g_log_stdout=${cli_LOG_STDOUT};
      ;;
    -x | --log-disable)
      cli_LOG_DISABLE=1; g_log_disable=${cli_LOG_DISABLE};
      ;;
    -d | --debug)
      if [[ ${_params_count} -eq 2 ]]; then
        echo "-d option without additional options"
        usage;
        exit 1;
      fi
      cli_DEBUG=1; g_debug=${cli_DEBUG};
      ;;
    -f | --force)
      cli_FORCEEXEC=1; g_forceexec=${cli_FORCEEXEC};
      if [[ ${g_debug} -ne 0 ]]; then
        echo "-f option not implemented";
      fi
      ;;
    -v | --version)
      if [[ ( ${_params_count} -eq 3 && ${g_debug} -eq 0 ) || ( ${_params_count} -gt 3 ) ]]; then
        if [ "${g_debug}" -ne 0 ]; then
          echo "-v option with additional options";
        fi
        usage;
        exit 1;
      fi
      version
      exit 0
      ;;
    -h | --help)
      if [[ ( ${_params_count} -eq 3 && ${g_debug} -eq 0 ) || ( ${_params_count} -gt 3 ) ]]; then
        if [ "${g_debug}" -ne 0 ]; then
          echo "-h option with additional options";
        fi
        usage;
        exit 1;
      fi
      help
      exit 0
      ;;
    -z | --show-settings)
       cli_SHOWVARS=1; g_show_vars=${cli_SHOWVARS};
       ;;
    --)
      shift;
      break;
      ;;
  esac
  shift
done

# bail out if user hasn't specified any options
if [[ ${_params_count} -eq 1 ]]; then
  echo
  echo "ABORTING. You have not specified any actionable options."
  echo
  usage;
  exit 1;
fi

argavail=$#
argparse=0
arglines_array=()

# parse args or parse redirected input lines (read stdin if no args)
if [[ ${g_debug} -ne 0 ]]; then
  echo "DEBUG: cliargs: ${argavail}"
fi

# max args we accept is 1: an ini file for input
if [[ ${g_use_config} -eq 0 &&  ${argavail} -gt 0 ]] \
  || [[ ${g_use_config} -eq 1 && ${argavail} -gt 1 ]]; then
  echo
  echo "ABORTING. Too many additional arguments specified."
  echo
  usage
  exit 1
fi

if [[ ${g_use_config} -eq 1 ]]; then
  if [[ ${g_debug} -ne 0 ]]; then
    echo "DEBUG: Updating config with ini file parameters."
  fi
  _lineRegex1="^[[:blank:]#;]+.*$"
  _lineRegex2="^[a-zA-Z_\-]+([=]{0}|[=]{2,})[a-zA-Z0-9\_\!\"\.\/\-]*$"

  if [[ ${argavail} -gt 0 ]]; then
    if [[ ${g_debug} -ne 0 ]]; then
      echo "DEBUG: Parsing ini data via direct file access."
    fi

    # args available; parse them
    while [[ ${argavail} -gt 0 ]]; do
      (( argparse++ ))
      if [[ ${g_debug} -ne 0 ]]; then
        printf "DEBUG: %02d: -|%s\n" "${argparse}" "${1}"
      fi
      arglines_array+=("${1}")
      shift
      argavail=$#
    done

    # check file access
    if [[ -f "${g_path_infile}" ]] && [[ ! -r "${g_path_infile}" ]]; then
      if [[ ${g_debug} -ne 0 ]]; then
        echo "DEBUG: ABORTING. Unable to access input file: ${g_path_infile}"
        echo "DEBUG: Must be a read permissions error."
      fi
      exit 1
    elif [[ ! -f "${g_path_infile}" ]]; then
      if [[ ${g_debug} -ne 0 ]]; then
        echo "DEBUG: ABORTING. Unable to access input file: ${g_path_infile}"
        echo "DEBUG: The target file appears to be missing."
      fi
      exit 1
    fi

    # grab iniPath and reset arglines_array for reuse
    _iniPath="${arglines_array[0]}"
    arglines_array=()
    argparse=0
    # read file and stuff each line in arglines_array
    while IFS= read -r line; do
      # skip:
      #  - empty lines,
      #  - lines starting with a space, hash, semicolon
      if [[ -z "${line}" || "${line}" =~ ${_lineRegex1} ]]; then
        if [[ ${g_debug} -ne 0 ]]; then
          printf "DEBUG: !!: -|%s (blank or comment, skipping)\n" "${line}"
        fi
        continue;
      fi
      # skip:
      #  - lines that don't contain exactly one '='
      #
      # NOTE: This is a syntax error in the ini file.
      #
      #
      if [[ "${line}" =~ ${_lineRegex2} ]]; then
        if [[ ${g_debug} -ne 0 ]]; then
          printf "DEBUG: !!: -|%s (malformed, skipping)\n" "${line}"
        fi
        continue;
      fi
      (( argparse++ ))
      if [[ ${g_debug} -ne 0 ]]; then
        printf "DEBUG: %02d: -|%s\n" "${argparse}" "${line}"
      fi
      arglines_array+=("${line}")
    done <<< "$(${g_path_cat} ${_iniPath})"

    unset _iniPath
  else
    if [[ ${g_debug} -ne 0 ]]; then
      echo "DEBUG: Parsing ini data via input redirection."
    fi

    # no args, must be stdin; read lines
    while IFS= read -r line; do
      # skip:
      #  - empty lines,
      #  - lines starting with a space, hash, semicolon
      if [[ -z "${line}" || "${line}" =~ ${_lineRegex1} ]]; then
        if [[ ${g_debug} -ne 0 ]]; then
          printf "DEBUG: !!: -|%s (blank or comment, skipping)\n" "${line}"
        fi
        continue;
      fi
      # skip:
      #  - lines that don't contain exactly one '='
      #
      # NOTE: This is a syntax error in the ini file.
      #
      #
      if [[ "${line}" =~ ${_lineRegex2} ]]; then
        if [[ ${g_debug} -ne 0 ]]; then
          printf "DEBUG: !!: -|%s (malformed, skipping)\n" "${line}"
        fi
        continue;
      fi
      (( argparse++ ))
      if [[ ${g_debug} -ne 0 ]]; then
        printf "DEBUG: %02d: -|%s\n" "${argparse}" "${line}"
      fi
      arglines_array+=("${line}")
    done
  fi

  # update gConfig with ini values
  dumpConfig
  INIDM='_' readConfigFromArray gConfig arglines_array[@]
  dumpConfig

  unset _lineRegex1
  unset _lineRegex2
fi

# Rangle our vars
#

# update config array with parameters
if [[ ${g_debug} -ne 0 ]]; then
  echo "DEBUG: Updating config with cli parameters."
fi

if [ -n "${cli_PASS_ROOT}" ]; then
  gConfig['PASSENGER_ROOT']="${cli_PASS_ROOT}"
fi

if [ -n "${cli_PASS_RUBY}" ]; then
  gConfig['PASSENGER_RUBY']="${cli_PASS_RUBY}"
fi

if [ -n "${cli_APP_NAME}" ]; then
  gConfig['APP_SERVER_NAME']="${cli_APP_NAME}"
fi

if [ -n "${cli_APP_USER}" ]; then
  gConfig['APP_SERVER_USER']="${cli_APP_USER}"
fi

if [ -n "${cli_APP_ROOT}" ]; then
  gConfig['APP_SERVER_ROOT']="${cli_APP_ROOT}"
fi

if [ -n "${cli_APP_PENV}" ]; then
  gConfig['APP_SERVER_PENV']="${g_app_server_penv}"
fi

if [ -n "${cli_LOG_LEVEL}" ]; then
  gConfig['NGINX_LOG_LEVEL']="${g_log_level}"
fi

if [ -n "${cli_LOG_STDOUT}" ]; then
  gConfig['NGINX_ACCESS_LOG']="${g_path_stdout}"
  gConfig['NGINX_ERROR_LOG']="${g_path_stdout}"
fi

if [ -n "${cli_LOG_DISABLE}" ]; then
  gConfig['NGINX_ACCESS_LOG']="off"
fi

dumpConfig

# show settings if -z option is on
if [[ ${g_show_vars} -ne 0 ]]; then
  showSettings

  exit 0
fi

# check in-file and out-file access
if [[ ${g_debug} -ne 0 ]]; then
  echo "DEBUG: Checking in-file and out-file."
fi

if [ -n "${cli_INFILE}" ]; then
  g_path_infile="${cli_INFILE}";

  if [[ -f "${g_path_infile}" ]] && [[ ! -r "${g_path_infile}" ]]; then
    if [[ ${g_debug} -ne 0 ]]; then
      echo "DEBUG: ABORTING. Unable to access input file: ${g_path_infile}"
      echo "DEBUG: Must be a read permissions error."
    fi
    exit 1
  elif [[ ! -f "${g_path_infile}" ]]; then
    if [[ ${g_debug} -ne 0 ]]; then
      echo "DEBUG: ABORTING. Unable to access input file: ${g_path_infile}"
      echo "DEBUG: The target file appears to be missing."
    fi
    exit 1
  fi
fi

if [ -n "${cli_OUTFILE}" ]; then
  g_path_outfile="${cli_OUTFILE}";

  if [[ -f "${g_path_outfile}" ]] && [[ ! -w "${g_path_outfile}" ]]; then
    if [[ ${g_debug} -ne 0 ]]; then
      echo "DEBUG: ABORTING. Unable to access output file: ${g_path_outfile}"
      echo "DEBUG: Must be a write permissions error."
    fi
    exit 1
  elif [[ -f "${g_path_outfile}" ]]; then
    if [[ ${g_debug} -ne 0 ]]; then
      echo "DEBUG: Truncating output file at path: ${g_path_outfile}"
    fi

    _resp=$({ echo > "${g_path_outfile}"; } 2>&1 )
    _rslt=$?
    if [[ ${_rslt} -ne 0 ]]; then
      echo "ABORTING. Unable to truncate output file: ${g_path_outfile}"
      echo "Not sure what the problem is."
      exit 1
    fi
  elif [[ ! -f "${g_path_outfile}" ]]; then
    if [[ ${g_debug} -ne 0 ]]; then
      echo "DEBUG: Unable to access output file: ${g_path_outfile}"
      echo "DEBUG: The target file appears to be missing."
    fi

    # create output file
    _outfiledir=$(${g_path_dirname} "${g_path_outfile}")
    if [[ -d "${_outfiledir}" ]] && [[ $(isPathWriteable "${_outfiledir}") -ne 1 ]]; then
      if [[ ${g_debug} -ne 0 ]]; then
        echo "DEBUG: Unable to access output file directory: ${_outfiledir}"
        echo "DEBUG: Must be a write permissions error."
      fi
      exit 1
    elif [[ ! -d "${_outfiledir}" ]]; then
      if [[ ${g_debug} -ne 0 ]]; then
        echo "DEBUG: Unable to access output file directory: ${_outfiledir}"
        echo "DEBUG: The target directory appears to be missing."
      fi
      exit 1
    fi

    if [[ ${g_debug} -ne 0 ]]; then
      echo "DEBUG: Creating output file at path: ${g_path_outfile}"
    fi

    _resp=$({ ${g_path_touch} "${g_path_outfile}"; } 2>&1 )
    _rslt=$?
    if [[ ${_rslt} -ne 0 ]]; then
      echo "ABORTING. Unable to create output file: ${g_path_outfile}"
      echo "Not sure what the problem is."
      exit 1
    fi
    unset _rslt
    unset _resp
    unset _outfiledir
  fi
fi

# parse the infile and replace tokens
_tmpConfigFile=$(TMPMK=1 mkTempFile "${g_path_worktemp}")
if [[ -z "${_tmpConfigFile}" || $? -ne 0 ]]; then
  echo "ABORTING. Unable to create temporary working file in: ${g_path_worktemp}"
  echo "Not sure what the problem is."
  exit 1
fi
if [[ ${g_debug} -ne 0 ]]; then
  echo "DEBUG: Created temporary working file: ${_tmpConfigFile}"
fi
${g_path_cat} "${g_path_infile}" >> ${_tmpConfigFile}
# JINX100
IFS=$'\n' read -r -d '' _jinx100 <<'EOF'
This file was auto-generated with jinx-config.\
# https://bitbucket.org/tapatip/jinx-config
EOF
${g_path_sed} -i.bak "s|%%JINX100%%|${_jinx100}|g" "${_tmpConfigFile}"
# NGINX_ACCESS_LOG
${g_path_sed} -i.bak "s|%%NGINX_ACCESS_LOG%%|${gConfig['NGINX_ACCESS_LOG']}\;|g" "${_tmpConfigFile}"
# NGINX_ERROR_LOG
${g_path_sed} -i.bak "s|%%NGINX_ERROR_LOG%%|${gConfig['NGINX_ERROR_LOG']} ${gConfig['NGINX_LOG_LEVEL']}\;|g" "${_tmpConfigFile}"
# PASSENGER_ROOT
${g_path_sed} -i.bak "s|%%PASSENGER_ROOT%%|${gConfig['PASSENGER_ROOT']}\;|g" "${_tmpConfigFile}"
# PASSENGER_RUBY
${g_path_sed} -i.bak "s|%%PASSENGER_RUBY%%|${gConfig['PASSENGER_RUBY']}\;|g" "${_tmpConfigFile}"
# APP_SERVER_NAME
${g_path_sed} -i.bak "s|%%APP_SERVER_NAME%%|${gConfig['APP_SERVER_NAME']}\;|g" "${_tmpConfigFile}"
# APP_SERVER_ROOT
${g_path_sed} -i.bak "s|%%APP_SERVER_ROOT%%|${gConfig['APP_SERVER_ROOT']}|g" "${_tmpConfigFile}"
# APP_SERVER_USER
${g_path_sed} -i.bak "s|%%APP_SERVER_USER%%|${gConfig['APP_SERVER_USER']}\;|g" "${_tmpConfigFile}"
# APP_SERVER_PENV
${g_path_sed} -i.bak "s|%%APP_SERVER_PENV%%|${gConfig['APP_SERVER_PENV']}\;|g" "${_tmpConfigFile}"
unset _jinx100

if [[ -n "${g_path_outfile}" ]]; then
  _resp=""
  _rslt=1
  #
  # copy to outfile, we use append here
  #
  if [[ ${g_debug} -ne 0 ]]; then
    echo "DEBUG: Copying configuration to output file at path: ${g_path_outfile}"
  fi
  _resp=$({ ${g_path_cat} "${_tmpConfigFile}" >> "${g_path_outfile}"; } 2>&1)
  _rslt=$?
  if [[ ${_rslt} -ne 0 ]]; then
    echo "ABORTING. Unable to copy configuration to output file at path: ${g_path_outfile}"
    echo "Not sure what the problem is."
    exit 1
  fi
  unset _resp
else
  _resp=""
  #
  # echo to stdout
  #
  if [[ ${g_debug} -ne 0 ]]; then
    echo "DEBUG: Copying configuration to stdout..."
  fi
  # capture stderr, let stdout through
  { _resp=$(${g_path_cat} ${_tmpConfigFile} 2>&1 1>&3-) ;} 3>&1
  unset _resp
fi

# remove tempfile and directory
if [[ ${g_debug} -ne 0 ]]; then
  echo "DEBUG: Removing temporary working directory: ${g_path_worktemp}"
fi
${g_path_rm} -rf "${g_path_worktemp}" &> /dev/null
if [[ $? -ne 0 ]]; then
  echo "ABORTING. Unable to remove temporary working directory: ${g_path_worktemp}"
  echo "Not sure what the problem is."
  exit 1
fi

exit 0
